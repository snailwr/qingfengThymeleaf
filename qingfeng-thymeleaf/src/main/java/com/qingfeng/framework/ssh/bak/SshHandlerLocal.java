//package com.qingfeng.framework.ssh;
//
//import cn.hutool.core.io.IoUtil;
//import cn.hutool.core.thread.ThreadUtil;
//import cn.hutool.core.util.StrUtil;
//import cn.hutool.extra.ssh.ChannelType;
//import cn.hutool.extra.ssh.JschUtil;
//import com.jcraft.jsch.ChannelShell;
//import com.jcraft.jsch.JSchException;
//import com.jcraft.jsch.Session;
//import com.qingfeng.util.CommandUtil;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.stereotype.Component;
//
//import javax.annotation.PostConstruct;
//import javax.websocket.OnClose;
//import javax.websocket.OnError;
//import javax.websocket.OnMessage;
//import javax.websocket.OnOpen;
//import javax.websocket.server.ServerEndpoint;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.OutputStream;
//import java.util.Arrays;
//import java.util.concurrent.ConcurrentHashMap;
//import java.util.concurrent.CopyOnWriteArraySet;
//import java.util.concurrent.atomic.AtomicInteger;
//
///**
// * ssh 处理2
// *
// * @author bwcx_jzy
// * @date 2019/8/9
// */
//@ServerEndpoint(value = "/ws/ssh2")
//@Component
//public class SshHandlerLocal {
//	private static final ConcurrentHashMap<String, HandlerItem> HANDLER_ITEM_CONCURRENT_HASH_MAP = new ConcurrentHashMap<>();
//
//	@PostConstruct
//	public void init() {
//		System.out.println("websocket 加载");
//	}
//	private static Logger log = LoggerFactory.getLogger(SshHandlerLocal.class);
//	private static final AtomicInteger OnlineCount = new AtomicInteger(0);
//	// concurrent包的线程安全Set，用来存放每个客户端对应的Session对象。
//	private static CopyOnWriteArraySet<javax.websocket.Session> SessionSet = new CopyOnWriteArraySet<javax.websocket.Session>();
//
//
//	/**
//	 * 连接建立成功调用的方法
//	 */
//	@OnOpen
//	public void onOpen(javax.websocket.Session session) throws Exception {
//		SessionSet.add(session);
//		int cnt = OnlineCount.incrementAndGet(); // 在线数加1
//		log.info("有连接加入，当前连接数为：{},sessionId={}", cnt,session.getId());
//		SendMessage(session, "连接成功，sessionId="+session.getId());
//		HandlerItem handlerItem = new HandlerItem(session);
//		HANDLER_ITEM_CONCURRENT_HASH_MAP.put(session.getId(), handlerItem);
//	}
//
//	/**
//	 * 连接关闭调用的方法
//	 */
//	@OnClose
//	public void onClose(javax.websocket.Session session) {
//		SessionSet.remove(session);
//		int cnt = OnlineCount.decrementAndGet();
//		log.info("有连接关闭，当前连接数为：{}", cnt);
//	}
//
//	/**
//	 * 收到客户端消息后调用的方法
//	 *
//	 * @param message
//	 *            客户端发送过来的消息
//	 */
//	@OnMessage
//	public void onMessage(String message, javax.websocket.Session session) throws Exception {
//		log.info("来自客户端的消息：{}",message);
////        SendMessage(session, "收到消息，消息内容："+message);
//		HandlerItem handlerItem = HANDLER_ITEM_CONCURRENT_HASH_MAP.get(session.getId());
//		this.sendCommand(handlerItem, message);
//	}
//
//	/**
//	 * 出现错误
//	 * @param session
//	 * @param error
//	 */
//	@OnError
//	public void onError(javax.websocket.Session session, Throwable error) {
//		log.error("发生错误：{}，Session ID： {}",error.getMessage(),session.getId());
//		error.printStackTrace();
//	}
//
//	private void sendCommand(HandlerItem handlerItem, String data) throws Exception {
//		handlerItem.sendMessage(data);
//	}
//
//	/**
//	 * 发送消息，实践表明，每次浏览器刷新，session会发生变化。
//	 * @param session
//	 * @param message
//	 */
//	public static void SendMessage(javax.websocket.Session session, String message) {
//		try {
////            session.getBasicRemote().sendText(String.format("%s (From Server，Session ID=%s)",message,session.getId()));
//			session.getBasicRemote().sendText(message);
//		} catch (IOException e) {
//			log.error("发送消息出错：{}", e.getMessage());
//			e.printStackTrace();
//		}
//	}
//
//	private class HandlerItem {
//		private final javax.websocket.Session session;
//		private final StringBuilder nowLineInput = new StringBuilder();
//
//		HandlerItem(javax.websocket.Session session) throws IOException {
//			this.session = session;
//		}
//
//		/**
//		 * 添加到命令队列
//		 *
//		 * @param msg 输入
//		 * @return 当前待确认待所有命令
//		 */
//		private String append(String msg) {
//			char[] x = msg.toCharArray();
//			if (x.length == 1 && x[0] == 127) {
//				// 退格键
//				int length = nowLineInput.length();
//				if (length > 0) {
//					nowLineInput.delete(length - 1, length);
//				}
//			} else {
//				nowLineInput.append(msg);
//			}
//			return nowLineInput.toString();
//		}
//
//		public void sendMessage(String msg) {
//			try {
//				session.getBasicRemote().sendText(msg);
//				String allCommand = this.append(msg);
//				if (StrUtil.equalsAny(msg, StrUtil.CR, StrUtil.TAB)) {
//					String join = nowLineInput.toString();
//					if (StrUtil.equals(msg, StrUtil.CR)) {
//						nowLineInput.setLength(0);
//						System.out.println("可执行命令-");
//						System.out.println(join);
//						String status = CommandUtil.execSystemCommand(join);
//						System.out.println("执行结果返回："+status);
//						session.getBasicRemote().sendText(status);
//					}
//				}
//			} catch (IOException e) {
//			}
//		}
//	}
//}
